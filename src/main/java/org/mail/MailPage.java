package org.mail;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by User on 06-Sep-16.
 */
public class MailPage extends AbstractPage {
    public WebDriver driver;

    //Login
    @FindBy(id = "loginEmail")
    private WebElement login;

    //Pass
    @FindBy (id = "loginPassword")
    private WebElement pass;

    //Button
    @FindBy (className = "btn btn-dark btn-lg btn-block")
    private WebElement button;

    public MailPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    private ChooseUser myUserLogin;
    public MailPage(ChooseUser myLogin) {
        this.myUserLogin = myLogin;
    }

    public MailPage enterUserData() {
        login.clear();
        login.sendKeys(myUserLogin);
        pass.clear();
        pass.sendKeys("ZXCasdqwe123");
        button.click();

        return this;
    }





}
