package org.mail;

import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by User on 05-Sep-16.
 */
public class ChooseUser {
    private static final String sFileName = "users.properties";
    private static String sDirSeparator = System.getProperty("file.separator");
    private static Properties props = new Properties();

    public void chooseUserLogin(WebDriver driver) throws IOException {
        File currentDir = new File(".");

        String sFilePath = currentDir.getCanonicalPath() + sDirSeparator + sFileName;
        FileInputStream ins = new FileInputStream(sFilePath);
        props.load(ins);
        System.out.println(props.getProperty("login:"));
        String userLogin = props.getProperty("login:");

    }
}